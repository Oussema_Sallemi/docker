(function($) {
  "use strict";

  /*---------------------------------
	Slider
	-----------------------------------*/
  $(".slider-two").slick({
    dots: false,
    arrows: false,
  });

  /*---------------------------------
	Product Thing Carouse
	-----------------------------------*/
  $(".product-thing").slick({
    dots: false,
    nextArrow: '<i class="fa fa-angle-right arrow-right arrow-button"></i>',
    prevArrow: '<i class="fa fa-angle-left arrow-left arrow-button"></i>',
    slidesToShow: 4,
    infinite: true,
    responsive: [
      {
        breakpoint: 0,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
        },
      },
    ],
  });

  /*---------------------------------
	Brand Logo Carousel
	-----------------------------------*/
  $(".brand-logo").slick({
    dots: false,
    arrows: false,
    slidesToShow: 6,
    responsive: [
      {
        breakpoint: 0,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
        },
      },
    ],
  });
  /*---------------------------------
	Brand Logo Carousel
	-----------------------------------*/
  $(".shipping-content").slick({
    dots: false,
    arrows: false,
    slidesToShow: 6,
    responsive: [
      {
        breakpoint: 0,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6,
        },
      },
    ],
  });
  // filtre mobile
  $(".filter-mobile").click(function() {
    $(".widget-background").slideToggle();
  });
  /*---------------------------------
Product Details Slider
-----------------------------------*/
  $(".product-slider-container").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<i class="fa fa-angle-right arrow-right arrow-button"></i>',
    prevArrow: '<i class="fa fa-angle-left arrow-left arrow-button"></i>',
    fade: true,
    accessibility: false,
  });
  $(".product-details-thumbnail").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".product-slider-container",
    nextArrow: '<i class="fa fa-angle-right arrow-right arrow-button"></i>',
    prevArrow: '<i class="fa fa-angle-left arrow-left arrow-button"></i>',
    dots: false,
    centerMode: false,
    focusOnSelect: false,
    responsive: [
      {
        breakpoint: 340,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 6,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        },
      },
    ],
  });
  $(".product-details-thumbnail,.slick-slide").on("click", function(event) {
    $(".product-slider-container").slick(
      "slickGoTo",
      $(this).data("slickIndex")
    );
  });
  /*---------------------------------
   Magnific Popup Activation
   -----------------------------------*/
  $(".product-slider-container").magnificPopup({
    delegate: "a", // child items selector, by clicking on it popup will open
    type: "image",
    gallery: {
      enabled: true,
    },
  });
})(jQuery);
