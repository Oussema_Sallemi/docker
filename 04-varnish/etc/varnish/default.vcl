vcl 4.0;
import std;
# The minimal Varnish version is 4.0 For SSL offloading, pass the following header in your proxy server or load balancer: 'X-Forwarded-Proto:
# https'
backend default {
    .host = "172.17.0.5";
    .port = "80";
    #.connect_timeout = 3s;
    .first_byte_timeout = 900s;
    #.between_bytes_timeout = 300s;
    .probe = {
        #.url = "/health_check.php";
        .timeout = 10s;
        .interval = 30s;
        .window = 3;
        .threshold = 2;
    }
}

include "/etc/varnish/evolix.vcl";
include "/etc/varnish/lepape.vcl";
