import { createApp } from "./app";

const { app, router } = createApp({ state: window.__INITIAL_STATE__ });

import "./assets/style/main.scss";
import "./assets/style/font-awesome.min.scss";
import "./assets/style/plugins.scss";
import "./assets/js/main.js";

router.onReady(() => {
  app.$mount("#app");
});
