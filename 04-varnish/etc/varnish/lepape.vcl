sub vcl_recv {
    if (req.method == "PURGE") {
        ban("req.http.host ~ .*");
        return (synth(200, "Full cache cleared"));
    }

    # Bypass health check requests
    if (req.url ~ "/health_check.php") {
        return (pass);
    }

    if (req.http.Accept-Encoding) {
        if (req.url ~ "\.(js|css)(\?.*|)$") {
            unset req.http.Accept-Encoding;
        }

        if (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        }
        elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        }
        else {
            unset req.http.Accept-Encoding;
        }
    }


    if (req.url ~ "/newsletter" || req.url ~ "/sales" || req.url ~ "/customer" || req.url ~ "/checkout" || req.url ~ "/onestepcheckout" ||req.url ~ "/chronorelais"  || req.url ~ "/reassurance" || req.url ~ "/ajax" || req.url ~ "/index.php/magento" || req.url ~ "lepape_payment" || req.url ~ "hipay" || req.url ~ "api" || req.url ~ "paypal" || req.url ~ "^/media/wysiwyg/bannieres-affiliation/") {
        return (pass);
    }else {
        unset req.http.cookie;
    }
}

sub vcl_backend_response {
    set beresp.ttl = 604800s;
    if (bereq.url ~ "/newsletter" || bereq.url ~ "/sales" || bereq.url ~ "/customer" || bereq.url ~ "/checkout"  || bereq.url ~ "/onestepcheckout" || bereq.url ~ "/chronorelais"  || bereq.url ~ "/reassurance" || bereq.url ~ "/ajax" || bereq.url ~ "/index.php/magento" || bereq.url ~ "lepape_payment" || bereq.url ~ "hipay" || bereq.url ~ "api" || bereq.url ~ "paypal" || bereq.url ~ "^/media/wysiwyg/bannieres-affiliation/") {
        set beresp.uncacheable = true;
        set beresp.ttl = 0s;
        return (deliver);
    } else {
        unset beresp.http.set-cookie;
    }

    if (beresp.http.content-type ~ "text/javascript|text/css|image/.*") {
        set beresp.do_gzip = true;
        unset beresp.http.Content-Length;
        return (deliver);
    }
}
