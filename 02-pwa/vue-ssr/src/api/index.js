import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3500/",
});

const getProducts = () => instance.get("/api/products");

export default {
  getProducts,
};
