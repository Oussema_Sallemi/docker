import Vue from "vue";
import Router from "vue-router";
import Entreprise from "../components/entreprise/index.vue";
import Home from "../pages/index.vue";

Vue.use(Router);

const products = [
  { id: 1, url: "cascadia-13" },
  { id: 2, url: "s-lab-sense-7" },
  { id: 3, url: "light-down-5" },
  { id: 4, url: "etip-glove" },
];

const categories = [
  { id: 1, url: "chaussures" },
  { id: 2, url: "vetements" },
  { id: 3, url: "electronique" },
  { id: 4, url: "velo" },
  { id: 5, url: "nutrition" },
  { id: 6, url: "running-trail" },
  { id: 7, url: "cyclisme" },
  { id: 8, url: "outdoor" },
  { id: 9, url: "fitness" },
  { id: 10, url: "natation" },
];

export function createRouter() {
  const routes = [
    {
      path: "/",
      component: Home,
      name: "home",
    },
    {
      path: "/entreprise",
      component: Entreprise,
      name: "entreprise",
    },
  ];
  products.forEach((prod) =>
    routes.push({
      path: `/${prod.url}`,
      component: () => import("../components/product/index.vue"),
      name: `product_${prod.id}`,
    })
  );
  categories.forEach((categ) =>
    routes.push({
      path: `/${categ.url}`,
      component: () => import("../components/category/index.vue"),
      name: `category_${categ.id}`,
    })
  );
  return new Router({
    mode: "history",
    routes: routes,
  });
}
