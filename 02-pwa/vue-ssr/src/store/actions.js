import api from "../api";

export const getProducts = ({ commit }) =>
  api.getProducts().then((response) => commit("setProducts", response.data));
